# Smart Underwater Display Interface

For the easiest and quickest install it is recommended to install the registry server first, then the SUD server, making sure that both program
are run after install.  

When these two are run a terminal window will open, and once the program is running the terminal window will close.  

Once those two are installed then install the SUD Interface.  

Both the registry server and SUD server will start on reboot and run in the background, so that the only program that has to be run 
is the SUD Interface.

There will be an update for this program soon so that it is more robust, and requires less interaction.